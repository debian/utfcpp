utfcpp (4.0.5-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 4.0.5
  * d/p/0002-Recover-CMake-ARCH_INDEPENDENT...: drop merged patch

 -- João Vítor Motta Souto Maior <soutomaior950@gmail.com>  Thu, 15 Aug 2024 19:34:47 -0300

utfcpp (4.0.3-1~exp2) experimental; urgency=medium

  * QA upload.
  * Orphan the package. (See #1065736)

 -- Boyuan Yang <byang@debian.org>  Sat, 09 Mar 2024 10:25:34 -0500

utfcpp (4.0.3-1~exp1) experimental; urgency=medium

  * New upstream 4.0.x series release. (Closes: #1057732)
  * debian/libutfcpp-doc.examples: Dropped, no longer exist.
  * debian/rules: Rewrite to use upstream testsuite.
  * debian/clean: Also clean our generated test directory.
  * debian/patches:
    + 0002-Recover-CMake-ARCH_INDEPENDENT-usage-from-dbb2423248.patch:
      Add proposed patch to fix architecture-independent support.
      The old 0002 patch is dropped.
    + 0003-CMakeLists.txt-Prefer-usr-share-cmake-for-cmake-file.patch:
      Prefer using /usr/share/cmake/utf8cpp/ instead of
      /usr/share/utf8cpp/cmake/.

 -- Boyuan Yang <byang@debian.org>  Thu, 07 Dec 2023 21:12:10 -0500

utfcpp (3.2.5-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Mark binary packages as arch:all and M-A: foreign
    since architecture-independent build is supported in this project
    since CMake 3.14. (LP: #1932094)
  * debian/rules: Do not explicitly disable tests anymore.
  * debian/patches/0001-Add-back-vendor-ftest-test-framework.patch:
    Add back upstream-vendored ftest test framework (as git submodule)
    as a patch to allow post-build testing.
  * debian/patches/:
    + 0002-Install-CMake-files-to-arch-indep-location-when-ARCH.patch:
      Correctly install CMake files to arch-indep location when
      CMake files are architecture-independent.

 -- Boyuan Yang <byang@debian.org>  Wed, 27 Sep 2023 14:57:50 -0400

utfcpp (3.2.4-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 17 Aug 2023 17:41:11 -0400

utfcpp (3.2.3-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Tue, 10 Jan 2023 21:28:27 -0500

utfcpp (3.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 10 Nov 2022 15:15:29 -0500

utfcpp (3.2.1-2) unstable; urgency=medium

  * Reintroduce libutfcpp-doc:all package for compatibility.
  * Let libutfcpp-doc package provide README.md documentation.
  * Also install upstream-provided examples into libutfcpp-doc.

 -- Boyuan Yang <byang@debian.org>  Tue, 05 Oct 2021 07:57:10 -0400

utfcpp (3.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh packaging:
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.
    + Update homepage field.
    + Mark binary package as Multi-Arch: same.
    - Drop libutfcpp-doc package, upstream is merging the doc into README.
  * debian/upstream/metadata: Added.
  * debian/watch: Update to v4; monitor new GitHub upstream.
  * debian/rules: Modernize packaging.
  * debian/copyright: Refresh information.
  * debian/libutfcpp-dev.maintscript, debian/libutfcpp-dev.links: Add
    compat symlink to avoid breaking build-dependencies after upstream
    moved header file paths.

 -- Boyuan Yang <byang@debian.org>  Mon, 04 Oct 2021 23:21:57 -0400

utfcpp (2.3.4-2) unstable; urgency=medium

  * Take over package maintenance through ITS process. (Closes: #995661)

 -- Boyuan Yang <byang@debian.org>  Mon, 04 Oct 2021 07:20:22 -0400

utfcpp (2.3.4-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild the package.
  * debian/control: Update Vcs-* fields to use git packaging repo
    under Salsa Debian Group. (Closes: #966202)

 -- Boyuan Yang <byang@debian.org>  Wed, 02 Sep 2020 19:42:08 -0400

utfcpp (2.3.4-1) unstable; urgency=low

  * New upstream

 -- Mathieu Malaterre <malat@debian.org>  Tue, 09 Apr 2013 18:20:31 +0200

utfcpp (2.3.2-1) unstable; urgency=low

  * Initial Debian Upload (Closes: #552618)

 -- Mathieu Malaterre <malat@debian.org>  Sat, 26 Jan 2013 19:13:19 +0100
